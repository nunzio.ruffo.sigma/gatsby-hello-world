#  Gatsby Hello World

https://nunzio.ruffo.gitlab.io/gatsby-hello-world/items/

Crear projecto
`gatsby new project-name`

Correr en entorno de desarrollo

`gatsby develop`

- Que es JAMStack, Gatsby, ReactJS, Gitlab Pages
- Diferencia con Server Side Rendering
- Instalar Gatsby
- Configurar Gitlab Pages, gitlab-ci.yml, (pathPrefix en el *gatsby-config.js*)
- Instalar Dependencias SASS, Bulma, entre otros. (Agregar en *plugins*, en el *gatsby-config.js*)
- Estructura de Carpetas
- Actualizar Rutas del SASS
- React Components
- Gatsby Components, Link, SEO, Layout
- Desplegar