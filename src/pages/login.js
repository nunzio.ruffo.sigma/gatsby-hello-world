import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Login = () => (
  <Layout>
    <SEO title="Home" />
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-black">Login</h3>
                <div class="box">
                    <form>
                        <div class="field">
                            <div class="control">
                                <input class="input" type="email" placeholder="Your Email" autofocus=""></input>
                            </div>
                        </div>

                        <div class="field">
                            <div class="control">
                                <input class="input" type="password" placeholder="Your Password"></input>
                            </div>
                        </div>
                        <button class="button is-block is-primary is-fullwidth">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </Layout>
)

export default Login

{/* < */}