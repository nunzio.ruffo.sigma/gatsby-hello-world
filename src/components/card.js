import PropTypes from "prop-types"
import React from "react"
import daftPunkImage from "../images/daft-punk.jpg"

const Card = () => {
    return (
        <div class="card mb-3">
            <div class="card-content">
                <div class="media-content">
                    <p class="title is-4">John Smith</p>
                    <p class="subtitle is-6">@johnsmith</p>
                </div>
                <img className="image is-128x128" src={daftPunkImage} alt="A dog smiling in a party hat" />
                <div class="content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus nec iaculis mauris.
                </div>
            </div>
        </div>
    )
}

export default Card;